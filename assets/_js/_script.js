$(document).ready(function(){
	ScreensFullHeight();
	$(".app-control").on('click', function(){
		var action = $(this).attr("data-control");
		switch(action){
			case "menu-open":
				$("#app-menu").addClass("open");
				break;
			case "menu-close":
				$("#app-menu").removeClass("open");
				break;
			case "font-add":
				if(!$("#app").hasClass("font-3")){
					if($("#app").hasClass("font-1")){
						$("#app").removeClass("font-3");
						$("#app").removeClass("font-1");
						$("#app").addClass("font-2");
					}else{
						if($("#app").hasClass("font-2")){
							$("#app").removeClass("font-1");
							$("#app").removeClass("font-2");
							$("#app").addClass("font-3");
						}else{
							$("#app").removeClass("font-2");
							$("#app").removeClass("font-3");
							$("#app").addClass("font-1");
						}
					}					
				}
				break;
			case "font-reset":
				$("#app").removeClass("font-1");
				$("#app").removeClass("font-2");
				$("#app").removeClass("font-3");
				break;
			case "font-remove":
				if($("#app").hasClass("font-1")){
					$("#app").removeClass("font-1");
				}
				if($("#app").hasClass("font-2")){
					$("#app").removeClass("font-2");
					$("#app").addClass("font-1");
				}
				if($("#app").hasClass("font-3")){
					$("#app").removeClass("font-3");
					$("#app").addClass("font-2");
				}
				break;
			case "contrast-reset":
				$("#app").removeClass("contraste-1");
				$("#app").removeClass("contraste-2");
				break;
			case "contrast-1":
				$("#app").removeClass("contraste-2");
				$("#app").addClass("contraste-1");
				break;
			case "contrast-2":
				$("#app").removeClass("contraste-1");
				$("#app").addClass("contraste-2");
				break;
			case "back-topicos":
				ScreensResetView();
				$("#screen-content--topicos").addClass("active");
				break;
		}
	});
	$(".btn-app--goTo").on("click", function(e){
		e.preventDefault();
		var screen = $(this).attr("data-screen");
		ScreensResetView();
		$("#screen-content--"+screen).addClass('active');
	});
});

