/*
#################################
	ANCHOR SCROLL
#################################
*/
	function linkScroll(option){
		switch(option.hash){
			case "#":
				$("html, body").animate({ scrollTop: 0 }, 1000); 
				break;
			default:
				$("html, body").animate({ scrollTop: $(option.hash).offset().top }, 1000);
				$("html, body").animate({ scrollTop: $(option.hash).offset().top }, 1000);
				break;
		}
	}
/*
#################################
	//ANCHOR SCROLL
#################################
*/
/*
#################################
	SCREENS
#################################
*/
	//FULL HEIGHT
	function ScreensFullHeight(){
		var windowHeight 	= window.innerHeight,
			elements 		= document.getElementsByClassName('app-screen');
		for(var i = 0; i < elements.length; i++){
			var element = elements[i];
			element.style.height = windowHeight + "px";
		}
	}
	//REMOVE ACTIVE
	function ScreensResetView(){
		$('.app-screen').each(function() {
			if($(this).hasClass('active')){
				$(this).removeClass('active');
			}
		});
	}
/*
#################################
	//SCREENs
#################################
*/